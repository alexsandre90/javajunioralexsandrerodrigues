CREATE TABLE SETOR(
	ID BIGINT(20) PRIMARY KEY AUTO_INCREMENT,
    DESCRICAO VARCHAR(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO SETOR (DESCRICAO) values ('DESENVOLVIMENTO');
INSERT INTO SETOR (DESCRICAO) values ('REQUISITOS');
INSERT INTO SETOR (DESCRICAO) values ('SUSTENTACAO');
INSERT INTO SETOR (DESCRICAO) values ('RH');
INSERT INTO SETOR (DESCRICAO) values ('SUPORTE');