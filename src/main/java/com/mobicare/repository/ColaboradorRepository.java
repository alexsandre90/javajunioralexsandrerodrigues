package com.mobicare.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.mobicare.model.Colaborador;

@Repository
public interface ColaboradorRepository extends JpaRepository<Colaborador, Long> {

	Colaborador findByCpf(String cpf);

	@Query("select c from COLABORADOR c where c.setor.id = :setorId")
	List<Colaborador> findAllBySetor(@Param("setorId") Long id);

}
