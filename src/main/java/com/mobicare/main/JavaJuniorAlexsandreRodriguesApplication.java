package com.mobicare.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@ComponentScan(basePackages = { "com.mobicare.*" })
@EntityScan("com.mobicare.model") 
@EnableJpaRepositories("com.mobicare.repository")
@EnableCaching
public class JavaJuniorAlexsandreRodriguesApplication {

	public static void main(String[] args) {
		SpringApplication.run(JavaJuniorAlexsandreRodriguesApplication.class, args);
	}

}
