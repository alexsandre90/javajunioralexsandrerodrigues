package com.mobicare.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CPF;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
@Entity(name = "COLABORADOR")
public class Colaborador {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "NOME")
	@NotBlank(message = "O atributo NOME não pode ser vazio!")
	@Size(min = 4, max = 30, message = "Tamanho entre 4 e 30 caracteres!")
	private String nome;

	@CPF(message = "Formato do CPF é inválido!")
	@Column(name = "CPF")
	private String cpf;

	@Column(name = "EMAIL")
	@NotBlank(message = "O atributo E-MAIL não pode ser vazio!")
	@Email
	private String email;

	@Column(name = "TELEFONE")
	@NotBlank(message = "O atributo TELEFONE não pode ser vazio!")
	@Size(min = 9, max = 15, message = "Tamanho entre 9 e 15 caracteres!")
	private String telefone;

	@Column(name = "NASCIMENTO")
	@Temporal(TemporalType.DATE)
	private Calendar nascimento;

	@ManyToOne
	private Setor setor;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public Calendar getNascimento() {
		return nascimento;
	}

	public void setNascimento(String nascimento) {
		
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			Calendar cal = Calendar.getInstance();
			cal.setTime(sdf.parse(nascimento));
			this.nascimento = cal;
			
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	public Setor getSetor() {
		return setor;
	}

	public void setSetor(Setor setor) {
		this.setor = setor;
	}

}
