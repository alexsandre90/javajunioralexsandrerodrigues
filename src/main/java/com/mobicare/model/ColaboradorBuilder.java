package com.mobicare.model;

public class ColaboradorBuilder {

	Colaborador user = new Colaborador();

	public ColaboradorBuilder setNome(String nome) {
		user.setNome(nome);
		return this;
	}

	public ColaboradorBuilder setCpf(String cpf) {
		user.setCpf(cpf);
		return this;
	}

	public ColaboradorBuilder setEmail(String email) {
		user.setEmail(email);
		return this;
	}

	public ColaboradorBuilder setTelefone(String telefone) {
		user.setTelefone(telefone);
		return this;
	}
	
	public ColaboradorBuilder setSetor(Setor setor) {
		user.setSetor(setor);
		return this;
	}

	public ColaboradorBuilder setNascimento(String date) {
		user.setNascimento(date);
		return this;
	}
	
	public Colaborador build() {
		return user;
	}


	
}
