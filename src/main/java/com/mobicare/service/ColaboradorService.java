package com.mobicare.service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.mobicare.model.Colaborador;
import com.mobicare.model.Setor;
import com.mobicare.repository.ColaboradorRepository;

@Service
public class ColaboradorService {

	// variavel para configurar porcentagem aceitavel de colaboradores menores de
	// idade e idosos
	private final int PORCENTAGEM_MENOR_IDADE = 20;
	private final int PORCENTAGEM_IDOSO = 20;

	@Autowired
	private ColaboradorRepository repository;

	// MAP de colaboradores agroupado por setor onde o resultado eh armazenado em
	// cache
	@Cacheable("colaboradores")
	public Map<String, List<Colaborador>> getColaboresGroupBySetor() {
		List<Colaborador> colaboradores = repository.findAll();

		Map<String, List<Colaborador>> groupBy = colaboradores.stream()
				.collect(Collectors.groupingBy(c -> c.getSetor().getDescricao()));

		return groupBy;
	}

	// metodo para salvar novos colaboradores
	// eh feita a validacao para menores e idosos antes de persistir no banco
	// toda vez que um novo colaborador eh salvo, o cache eh limpo
	@CacheEvict(cacheNames = "colaboradores", allEntries = true)
	public void save(Colaborador colaborador) {
		int idade = obterIdade(colaborador.getNascimento());

		if (idade < 18) {

			int qtdMenor = menoresDeIdadePorSetor(colaborador.getSetor().getId()).size();
			int qtdTotal = findAllBySetor(colaborador.getSetor()).size();

			if (validarESalvarMenorDeIdade(colaborador, qtdMenor, qtdTotal)) {
				return;
			}
		}

		if (idade > 65) {

			int qtdIdosos = colaboradoresIdosos().size();
			int qtdTotal = repository.findAll().size();

			if (validarESalvarIdoso(colaborador, qtdIdosos, qtdTotal)) {
				return;
			}
		}
		repository.save(colaborador);
	}

	// metodo privado para fazer toda a logica de validacao do colaborador idoso
	private boolean validarESalvarIdoso(Colaborador colaborador, int qtdIdosos, int qtdTotal) {
		// Foi preciso criar a variavel e validar se eh maior que 0 (zero) para
		// evitar ArithmeticException na divisao
		if (qtdIdosos > 0) {

			if (qtdTotal > 0) {
				int dividir = qtdIdosos * 100 / qtdTotal;

				if (dividir > 0) {
					if (dividir < PORCENTAGEM_IDOSO) {
						repository.save(colaborador);
						return true;
					} else {
						throw new IllegalArgumentException("Limite de colaborador maior de 65 anos já excedido!");
					}
				}
			}
		}
		return false;

	}

	// meotod privado para fazer toda a logica de validacao de colaborador menor de
	// idade por setor
	private boolean validarESalvarMenorDeIdade(Colaborador colaborador, int qtdMenor, int qtdTotal) {
		// Foi preciso criar a variavel e validar se eh maior que 0 (zero) para
		// evitar ArithmeticException na divisao
		if (qtdMenor > 0) {

			if (qtdTotal > 0) {
				int dividir = qtdMenor * 100 / qtdTotal;

				if (dividir > 0) {
					if (dividir < PORCENTAGEM_MENOR_IDADE) {
						repository.save(colaborador);
						return true;
					} else {
						throw new IllegalArgumentException(
								"Limite de colaborador menor de 18 anos já excedido para esse setor!");
					}
				}
			}
		}
		return false;
	}

	// retorna lista de colaborador por setor recebendo um Colaborador como
	// parametro
	public List<Colaborador> findAllBySetor(Colaborador colaborador) {
		return repository.findAllBySetor(colaborador.getSetor().getId());
	}

	// retorn lista de colabordor por setor recebendo um Setor como parametro
	public List<Colaborador> findAllBySetor(Setor setor) {
		return repository.findAllBySetor(setor.getId());
	}

	// retorna lista de menores de idade por Setor recebendo o ID do setor
	public List<Colaborador> menoresDeIdadePorSetor(Long id) {
		List<Colaborador> menores = new ArrayList<>();

		for (Colaborador colaborador : repository.findAllBySetor(id)) {

			if (obterIdade(colaborador.getNascimento()) < 18) {
				menores.add(colaborador);
			}
		}

		return menores;
	}

	// retorna lista de idosos de todo o banco de dados
	public List<Colaborador> colaboradoresIdosos() {

		List<Colaborador> idosos = new ArrayList<>();

		for (Colaborador colaborador : repository.findAll()) {

			if (obterIdade(colaborador.getNascimento()) > 65) {
				idosos.add(colaborador);
			}
		}

		return idosos;
	}

	// metodo para deletar todos os colaboradores do banco
	// o cache eh limpo sempre que o metodo eh executado
	@CacheEvict(cacheNames = "colaboradores", allEntries = true)
	public void deletaAll() {
		repository.deleteAll();
	}

	// metodo para deletar o colaborador do banco
	// o cache eh limpo sempre que o metodo eh executado
	@CacheEvict(cacheNames = "colaboradores", allEntries = true)
	public void delete(Colaborador colaborador) {
		repository.delete(colaborador);
	}

	public Colaborador findById(long id) {
		return repository.findById(id).get();
	}

	// metodo para localizar o Colaborador por CPF ja que o CPF tem valor unico
	// muito util para as classes de testes
	public Colaborador findByCpf(String cpf) {
		return repository.findByCpf(cpf);
	}

	// metodo privado para obter a idade de um colaborador com base na data atual
	// (today) e na data de nascimento do colaborador
	private int obterIdade(Calendar nascimento) {
		Calendar today = Calendar.getInstance();

		int age = today.get(Calendar.YEAR) - nascimento.get(Calendar.YEAR);

		return age;
	}
}
