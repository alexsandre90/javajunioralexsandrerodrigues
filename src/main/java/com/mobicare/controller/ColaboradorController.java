package com.mobicare.controller;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import com.mobicare.model.Colaborador;
import com.mobicare.model.ColaboradorBuilder;
import com.mobicare.model.Setor;
import com.mobicare.service.ColaboradorService;

@RestController
@RequestMapping("/")
public class ColaboradorController {

	@Autowired
	private ColaboradorService service;

	@GetMapping
	public ResponseEntity<?> getAllColaboradores() {
		Map<String, List<Colaborador>> colaboradores = service.getColaboresGroupBySetor();

		return new ResponseEntity<>(colaboradores, HttpStatus.OK);

	}

	
	//obter colaborador por ID.
	//uma excecao e lancada com status 404 caso o colaborador nao seja encontrado.
	//caso haja sucesso eh retornado o status 200
	@GetMapping("{id}")
	public ResponseEntity<?> getColaboradorById(@PathVariable Long id) {
		Colaborador colaborador = null;

		try {
			colaborador = service.findById(id);

		} catch (NoSuchElementException e) {
			throw new HttpClientErrorException(HttpStatus.NOT_FOUND,
					"Não foi localizado Colaborador com o ID informado!");
		}

		return new ResponseEntity<>(colaborador, HttpStatus.OK);
	}

	//metodo para salvar com o verbo POST e retorno status code 201
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> addColaborador(@RequestBody @Valid Colaborador colaborador) {

		service.save(colaborador);

		return new ResponseEntity<>(colaborador, HttpStatus.CREATED);
	}

	//deletar colaborador por ID.
	//uma excecao eh lancada com status 404 caso o colaborador nao seja encontrado.
	//caso haja sucesso e retornado o status 200
	@DeleteMapping("{id}")
	public ResponseEntity<?> deleteColaboradorById(@PathVariable Long id) {

		Colaborador colaborador = null;

		try {
			colaborador = service.findById(id);

		} catch (NoSuchElementException e) {
			throw new HttpClientErrorException(HttpStatus.NOT_FOUND,
					"Não foi localizado Colaborador com o ID informado!");
		}

		service.delete(colaborador);

		return new ResponseEntity<>(HttpStatus.OK);
	}

	
	//metodo para criar varios colaboradores, de varios setores e idades.
	//util para pode validar a aplicacao
	//se a URI for executada diversas vezes havera exception de integridade no banco pois alguns campos sao unicos (CPF e e-mail)
	@GetMapping("createAll")
	public ResponseEntity<?> createAll() {
		Colaborador user1 = new ColaboradorBuilder().setNome("José").setCpf("840.347.220-02").setEmail("jose@jose.com")
				.setTelefone("1111-1111").setSetor(new Setor(1l)).setNascimento("01/12/1980").build();
	
		Colaborador user2 = new ColaboradorBuilder().setNome("Carlos").setCpf("441.071.640-98").setEmail("carlos@carlos.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("14/01/1975").build();
		
		Colaborador user3 = new ColaboradorBuilder().setNome("Antonio").setCpf("411.189.170-57").setEmail("antonio@antonio.com")
				.setTelefone("1111-1111").setSetor(new Setor(1L)).setNascimento("18/04/1990").build();
		
		Colaborador user4 = new ColaboradorBuilder().setNome("Pedro").setCpf("510.409.350-60").setEmail("pedro@pedro.com")
				.setTelefone("1111-1111").setSetor(new Setor(5l)).setNascimento("28/02/1995").build();
		
		Colaborador user5 = new ColaboradorBuilder().setNome("Gelson").setCpf("127.786.430-60").setEmail("gelson@gelson.com")
				.setTelefone("1111-1111").setSetor(new Setor(2l)).setNascimento("30/09/1985").build();
		
		Colaborador user6 = new ColaboradorBuilder().setNome("João").setCpf("272.116.010-94").setEmail("joao@joao.com")
				.setTelefone("1111-1111").setSetor(new Setor(1l)).setNascimento("01/12/1980").build();
	
		Colaborador user7 = new ColaboradorBuilder().setNome("Charles").setCpf("076.376.590-20").setEmail("charles@carlos.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("14/01/1975").build();
		
		Colaborador user8 = new ColaboradorBuilder().setNome("Alex").setCpf("312.237.200-20").setEmail("antonio11@antonio.com")
				.setTelefone("1111-1111").setSetor(new Setor(1L)).setNascimento("18/04/2004").build();
		
		Colaborador user9 = new ColaboradorBuilder().setNome("Paulo").setCpf("857.060.510-20").setEmail("paulo@pedro.com")
				.setTelefone("1111-1111").setSetor(new Setor(3l)).setNascimento("01/07/2004").build();
		
		Colaborador user10 = new ColaboradorBuilder().setNome("Gabriel").setCpf("262.427.390-54").setEmail("gabriel@gelson.com")
				.setTelefone("1111-1111").setSetor(new Setor(2l)).setNascimento("30/09/1970").build();
		
		Colaborador user11 = new ColaboradorBuilder().setNome("Josélias").setCpf("495.373.370-30").setEmail("joselias@jose.com")
				.setTelefone("1111-1111").setSetor(new Setor(3l)).setNascimento("01/12/1980").build();
	
		Colaborador user12 = new ColaboradorBuilder().setNome("Cacalo").setCpf("920.916.680-95").setEmail("cacalo@carlos.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("14/01/1940").build();
		
		Colaborador user13 = new ColaboradorBuilder().setNome("Fernando").setCpf("749.750.270-03").setEmail("antonio123@antonio.com")
				.setTelefone("1111-1111").setSetor(new Setor(3L)).setNascimento("18/04/1990").build();
		
		Colaborador user14 = new ColaboradorBuilder().setNome("Thiago").setCpf("616.457.510-90").setEmail("pedr44o@pedro.com")
				.setTelefone("1111-1111").setSetor(new Setor(5l)).setNascimento("28/02/1940").build();
		
		Colaborador user15 = new ColaboradorBuilder().setNome("Diego").setCpf("241.097.570-45").setEmail("gels22on@gelson.com")
				.setTelefone("1111-1111").setSetor(new Setor(2l)).setNascimento("30/09/1935").build();
		
		service.save(user1);
		service.save(user2);
		service.save(user3);
		service.save(user4);
		service.save(user5);
		service.save(user6);
		service.save(user7);
		service.save(user8);
		service.save(user9);
		service.save(user10);
		service.save(user11);
		service.save(user12);
		service.save(user13);
		service.save(user14);
		service.save(user15);

		return new ResponseEntity<>(HttpStatus.OK);
	}

}
