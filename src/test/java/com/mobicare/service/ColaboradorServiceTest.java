package com.mobicare.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.mobicare.main.JavaJuniorAlexsandreRodriguesApplication;
import com.mobicare.model.Colaborador;
import com.mobicare.model.ColaboradorBuilder;
import com.mobicare.model.Setor;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ComponentScan("com.mobicare.model")
@SpringBootTest(classes = {JavaJuniorAlexsandreRodriguesApplication.class, ColaboradorService.class})
public class ColaboradorServiceTest {

	@Autowired
	private ColaboradorService service;
	
	
	@Before
	public void setUp() {
		Colaborador user1 = new ColaboradorBuilder().setNome("José").setCpf("840.347.220-02").setEmail("jose@jose.com")
				.setTelefone("1111-1111").setSetor(new Setor(1l)).setNascimento("01/12/1980").build();
	
		Colaborador user2 = new ColaboradorBuilder().setNome("Carlos").setCpf("441.071.640-98").setEmail("carlos@carlos.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("14/01/1975").build();
		
		Colaborador user3 = new ColaboradorBuilder().setNome("Antonio").setCpf("411.189.170-57").setEmail("antonio@antonio.com")
				.setTelefone("1111-1111").setSetor(new Setor(1L)).setNascimento("18/04/1990").build();
		
		Colaborador user4 = new ColaboradorBuilder().setNome("Pedro").setCpf("510.409.350-60").setEmail("pedro@pedro.com")
				.setTelefone("1111-1111").setSetor(new Setor(5l)).setNascimento("28/02/1995").build();
		
		Colaborador user5 = new ColaboradorBuilder().setNome("Gelson").setCpf("127.786.430-60").setEmail("gelson@gelson.com")
				.setTelefone("1111-1111").setSetor(new Setor(2l)).setNascimento("30/09/1985").build();
		
		Colaborador user6 = new ColaboradorBuilder().setNome("João").setCpf("272.116.010-94").setEmail("joao@joao.com")
				.setTelefone("1111-1111").setSetor(new Setor(1l)).setNascimento("01/12/1980").build();
	
		Colaborador user7 = new ColaboradorBuilder().setNome("Charles").setCpf("076.376.590-20").setEmail("charles@carlos.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("14/01/1975").build();
		
		Colaborador user8 = new ColaboradorBuilder().setNome("Alex").setCpf("312.237.200-20").setEmail("antonio11@antonio.com")
				.setTelefone("1111-1111").setSetor(new Setor(1L)).setNascimento("18/04/2004").build();
		
		Colaborador user9 = new ColaboradorBuilder().setNome("Paulo").setCpf("857.060.510-20").setEmail("paulo@pedro.com")
				.setTelefone("1111-1111").setSetor(new Setor(3l)).setNascimento("01/07/2004").build();
		
		Colaborador user10 = new ColaboradorBuilder().setNome("Gabriel").setCpf("262.427.390-54").setEmail("gabriel@gelson.com")
				.setTelefone("1111-1111").setSetor(new Setor(2l)).setNascimento("30/09/1970").build();
		
		Colaborador user11 = new ColaboradorBuilder().setNome("Josélias").setCpf("495.373.370-30").setEmail("joselias@jose.com")
				.setTelefone("1111-1111").setSetor(new Setor(3l)).setNascimento("01/12/1980").build();
	
		Colaborador user12 = new ColaboradorBuilder().setNome("Cacalo").setCpf("920.916.680-95").setEmail("cacalo@carlos.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("14/01/1940").build();
		
		Colaborador user13 = new ColaboradorBuilder().setNome("Fernando").setCpf("749.750.270-03").setEmail("antonio123@antonio.com")
				.setTelefone("1111-1111").setSetor(new Setor(3L)).setNascimento("18/04/1990").build();
		
		Colaborador user14 = new ColaboradorBuilder().setNome("Thiago").setCpf("616.457.510-90").setEmail("pedr44o@pedro.com")
				.setTelefone("1111-1111").setSetor(new Setor(5l)).setNascimento("28/02/1940").build();
		
		Colaborador user15 = new ColaboradorBuilder().setNome("Diego").setCpf("241.097.570-45").setEmail("gels22on@gelson.com")
				.setTelefone("1111-1111").setSetor(new Setor(2l)).setNascimento("30/09/1935").build();
		
		service.save(user1);
		service.save(user2);
		service.save(user3);
		service.save(user4);
		service.save(user5);
		service.save(user6);
		service.save(user7);
		service.save(user8);
		service.save(user9);
		service.save(user10);
		service.save(user11);
		service.save(user12);
		service.save(user13);
		service.save(user14);
		service.save(user15);
	}
	
	@Test
	public void testGroupBySetor() {
		Map<String, List<Colaborador>> groupBy = service.getColaboresGroupBySetor();
		
		assertEquals(4, groupBy.get("DESENVOLVIMENTO").size());
		
	}
	
	@Test
	public void testFindBySetor() {
		Colaborador colaborador = service.findByCpf("441.071.640-98");
		List<Colaborador> colaboradores = service.findAllBySetor(colaborador);
		
		assertEquals(3, colaboradores.size());
	}
	
	@Test
	public void testBuscarColaborador() {
		Colaborador colaborador = service.findByCpf("840.347.220-02");
		
		assertEquals("José", colaborador.getNome());
				
	}
	
	@Test
	public void testCacheListaDeColaboradores() {
		Map<String, List<Colaborador>> lista1 = service.getColaboresGroupBySetor();
		Map<String, List<Colaborador>> lista2 = service.getColaboresGroupBySetor();
		
		assertEquals(lista1, lista2);
	}
	
	@Test
	public void testCacheEvictComListaDeColaboradores() {
		Map<String, List<Colaborador>> lista1 = service.getColaboresGroupBySetor();
		
		Colaborador colaborador = service.findByCpf("411.189.170-57");
		
		service.delete(colaborador);
		
		Map<String, List<Colaborador>> lista2 = service.getColaboresGroupBySetor();
		
		assertFalse(lista1 == lista2);
	}
	
	@Test(expected = NullPointerException.class)
	public void testRemoverColaborador() {
		Colaborador colaborador = service.findByCpf("840.347.220-02");
		
		service.delete(colaborador);
		
		Colaborador colaborador2 = service.findByCpf("840.347.220-02");
		
		assertEquals("José", colaborador2.getNome());
	}
	
	@Test
	public void testeMenoresDeIdadePorSetor() {
		List<Colaborador> colaboradores = service.menoresDeIdadePorSetor(1L);
		
		assertEquals(1, colaboradores.size());
	}
	
	@Test
	public void testeMaioresDeIdadeDosColaboradores() {
		
		List<Colaborador> colaboradores = service.colaboradoresIdosos();
		
		assertEquals(3, colaboradores.size());
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeBloqueioCadastroColaboradorMenorDeIdadePorSetor() {
		Colaborador user16 = new ColaboradorBuilder().setNome("Maycon").setCpf("786.735.500-68").setEmail("maycon@jose.com")
				.setTelefone("1111-1111").setSetor(new Setor(1l)).setNascimento("01/12/2007").build();
		
		service.save(user16);
		
	}
	
	@Test(expected = IllegalArgumentException.class)
	public void testeBloqueioCadastroColaboradorIdoso() {
		Colaborador user17 = new ColaboradorBuilder().setNome("Teves").setCpf("816.259.000-58").setEmail("teves@teves.com")
				.setTelefone("1111-1111").setSetor(new Setor(4l)).setNascimento("01/12/1940").build();
		
		service.save(user17);
		
	}
	
	@After
	public void rollback() {
		service.deletaAll();
	}

}
