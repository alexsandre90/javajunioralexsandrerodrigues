package com.mobicare.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.text.SimpleDateFormat;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mobicare.main.JavaJuniorAlexsandreRodriguesApplication;
import com.mobicare.model.Colaborador;
import com.mobicare.model.ColaboradorBuilder;
import com.mobicare.model.Setor;
import com.mobicare.service.ColaboradorService;

@RunWith(SpringRunner.class)
@WebAppConfiguration
@ComponentScan("com.mobicare.model")
@SpringBootTest(classes = { JavaJuniorAlexsandreRodriguesApplication.class, ColaboradorService.class })
@AutoConfigureMockMvc
public class ColaboradorControllerTest {

	@Autowired
	private MockMvc mock;

	@Autowired
	private ColaboradorService service;

	@Test
	public void testUrlPrincipal() throws Exception {

		mock.perform(get("/").accept("application/json")).andExpect(status().isOk());
	}

	@Test
	public void testVerboPost() throws Exception {

		Colaborador colaborador = new ColaboradorBuilder().setNome("Felipe").setCpf("768.561.540-11")
				.setEmail("felipe@felipe.com").setTelefone("1111-1111").setNascimento("01/12/1990")
				.setSetor(new Setor(1l)).build();


		mock.perform(post("/").content(asJsonString(colaborador)).contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated());
	}

	@Test
	public void testGetComID() throws Exception {
		Colaborador colaborador = new ColaboradorBuilder().setNome("Felipe").setCpf("768.561.540-11")
				.setEmail("felipe@felipe.com").setTelefone("1111-1111").setNascimento("12/12/1990")
				.setSetor(new Setor(1l)).build();

		service.save(colaborador);

		Colaborador user1 = service.findByCpf("768.561.540-11");

		mock.perform(get("/{id}", user1.getId()).contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.nome").value("Felipe")).andExpect(status().isOk());

		service.delete(colaborador);
	}

	@Test
	public void testDeleteComId() throws Exception {
		Colaborador colaborador = new ColaboradorBuilder().setNome("Felipe").setCpf("768.561.540-11")
				.setEmail("felipe@felipe.com").setTelefone("1111-1111").setNascimento("12/12/1990")
				.setSetor(new Setor(1l)).build();

		service.save(colaborador);

		Colaborador user1 = service.findByCpf("768.561.540-11");

		mock.perform(delete("/{id}", user1.getId()).contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());
	}
	
	
	
	
	@After
	public void roolback() {
		service.deletaAll();
	}
	
	

	private String asJsonString(final Object obj) {

		try {
			// tratamento da data de nascimento para evitar exception na conversao para json
			SimpleDateFormat s = new SimpleDateFormat("dd/MM/yyyy");

			ObjectMapper mapper = new ObjectMapper();
			mapper.setDateFormat(s);

			String jsonContent = mapper.writeValueAsString(obj);

			return jsonContent;

		} catch (Exception e) {

			throw new RuntimeException(e);

		}
	}

}
